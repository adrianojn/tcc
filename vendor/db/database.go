package db

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

var (
	db     = sqlx.MustConnect("sqlite3", "db.sqlite")
	Get    = db.Get
	Exec   = db.Exec
	Select = db.Select
)

func init() {
	db.MustExec(`
	create table if not exists
	usuario (
		id       integer primary key autoincrement,
		nome     text not null,
		hash     text not null,
		email    text not null,
		aprovado integer not null
	);
	
	create table if not exists
	topico (
		id     integer primary key autoincrement,
		titulo text not null,
		texto  text not null
	);
	
	create table if not exists
	pergunta (
		id         integer primary key autoincrement,
		usuario    integer not null,
		topico     integer not null,
		titulo     text not null,
		texto      text not null,
		data       datetime default current_timestamp,

		foreign key (usuario) references usuario(id),
		foreign key (topico)  references topico(id)
	);
	
	create table if not exists
	resposta (
		id       integer primary key autoincrement,
		usuario  integer not null,
		pergunta integer not null,
		texto    text not null,
		marcada  integer not null,
		data     datetime default current_timestamp,

		foreign key (usuario)  references usuario(id),
		foreign key (pergunta) references pergunta(id)
	);
	`)
}
