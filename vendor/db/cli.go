package db

import "fmt"

func Aprovar(usuario string) {
	_, err := db.Exec("update usuario set aprovado = 1 where id = ?", usuario)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("OK")
	}
}

func NovoTopico(titulo string, texto string) {
	_, err := db.Exec("insert into topico (titulo, texto) values (? ,?)", titulo, texto)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("OK")
	}
}
