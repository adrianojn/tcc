package controller

import (
	"auth"
	"model"
	"net/http"
	"view"
)

func PerguntaTodas(w http.ResponseWriter, r *http.Request) {
	var v view.Model
	id := r.FormValue("id")
	v.Topico.ID = id
	perguntas, err := model.ListarPerguntasPorTopico(id)
	v.Perguntas = perguntas
	if err != nil {
		v.Erro = err.Error()
	}
	v.Render(w, "perguntas.html")
}

func PerguntaNova(w http.ResponseWriter, r *http.Request) {
	var v view.Model
	if r.Method == "GET" {
		v.Pergunta.Topico = r.FormValue("id")
		v.Render(w, "pergunta.html")
		return
	}
	v.Pergunta.Usuario = auth.Get(r)
	v.Pergunta.Topico = r.FormValue("topico")
	v.Pergunta.Titulo = r.FormValue("titulo")
	v.Pergunta.Texto = r.FormValue("texto")
	err := v.Pergunta.Salvar()
	if err != nil {
		v.Erro = err.Error()
		v.Render(w, "pergunta.html")
	} else {
		http.Redirect(w, r, "/perguntas?id="+r.FormValue("topico"), http.StatusFound)
	}
}
