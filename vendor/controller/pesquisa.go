package controller

import (
	"model"
	"net/http"
	"view"
)

// Pesquisar perguntas.
// Pesquisar perguntas e respostas marcadas.
// Pesquisar perguntas e todas as respostas.

func Pesquisa(w http.ResponseWriter, r *http.Request) {
	var v view.Model
	if r.Method == "GET" {
		v.Render(w, "pesquisa.html")
		return
	}
	texto := r.FormValue("q")
	switch r.FormValue("tipo") {
	case "perguntas":
		v.Perguntas = model.EncontarPerguntas(texto)
	case "marcadas":
		v.Perguntas = model.EncontarPerguntasERespostasMarcadas(texto)
	case "respostas":
		v.Perguntas = model.EncontarPerguntasERespostas(texto)
	}
	v.Render(w, "pesquisa.html")
}
