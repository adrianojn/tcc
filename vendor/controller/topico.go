package controller

import (
	"model"
	"net/http"
	"view"
)

func TopicoTodos(w http.ResponseWriter, r *http.Request) {
	var v view.Model
	topicos, err := model.ListarTodosTopicos()
	if err != nil {
		v.Erro = err.Error()
	}
	v.Topicos = topicos
	v.Render(w, "topicos.html")
}
