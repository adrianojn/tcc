package controller

import (
	"auth"
	"net/http"
	"view"
)

func Index(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		w.WriteHeader(404)
		return
	}
	var v view.Model
	v.LoggedIn = auth.Get(r) != ""
	v.Render(w, "index.html")
}

func Aguarde(w http.ResponseWriter, r *http.Request) {
	view.Render(w, "aguarde.html", nil)
}
