package controller

import (
	"bcrypt"
	"errors"
	"model"
	"net/http"
	"strings"
	"view"
)

func UsuarioTodos(w http.ResponseWriter, r *http.Request) {
	var v view.Model
	usuarios, err := model.ListarTodosUsuarios()
	if err != nil {
		v.Erro = err.Error()
	}
	v.Usuarios = usuarios
	v.Render(w, "usuarios.html")
}

func UsuarioNovo(w http.ResponseWriter, r *http.Request) {
	var v view.Model
	if r.Method == "GET" {
		v.Render(w, "usuario.html")
		return
	}
	v.Usuario.Nome = r.FormValue("nome")
	v.Usuario.Email = r.FormValue("email")
	hash, err := usuarioValidate(r)
	if err != nil {
		v.Erro = err.Error()
		v.Render(w, "usuario.html")
		return
	}
	v.Usuario.Hash = hash
	err = v.Usuario.Salvar()
	if err != nil {
		v.Erro = err.Error()
		v.Render(w, "usuario.html")
		return
	}
	http.Redirect(w, r, "/", http.StatusFound)
}

func usuarioValidate(r *http.Request) (string, error) {
	var (
		senha  = r.FormValue("senha")
		senha2 = r.FormValue("senha2")
	)
	if empty(senha) || empty(senha2) {
		return "", errors.New("Senha não pode estar em branco.")
	}
	if senha != senha2 {
		return "", errors.New("Senhas não são iguais.")
	}
	return bcrypt.Encode(senha)
}

func empty(s string) bool {
	return strings.TrimSpace(s) == ""
}
