package controller

import (
	"auth"
	"bcrypt"
	"log"
	"model"
	"net/http"
	"view"
)

func Entrar(w http.ResponseWriter, r *http.Request) {
	var v view.Model
	if r.Method == "GET" {
		v.Render(w, "entrar.html")
		return
	}

	email := r.FormValue("email")
	senha := r.FormValue("senha")

	usuario, err := model.EncontrarUsuarioPorEmail(email)
	v.Usuario = usuario
	if err != nil {
		log.Println("EMAIL", err)
	}
	if bcrypt.Check(senha, v.Usuario.Hash) {
		log.Println("LOGIN", email)
		auth.Save(w, v.Usuario.ID)
		http.Redirect(w, r, "/", http.StatusFound)
	} else {
		v.Erro = "E-mail ou senha inválida."
		v.Render(w, "entrar.html")
	}
}

func Sair(w http.ResponseWriter, r *http.Request) {
	auth.Delete(w)
	http.Redirect(w, r, "/", http.StatusFound)
}
