package controller

import "net/http"

func redirectHome(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/", http.StatusFound)
}

func redirectReferer(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, r.Header.Get("Referer"), http.StatusFound)
}

func redirectAguarde(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/aguarde", http.StatusFound)
}
