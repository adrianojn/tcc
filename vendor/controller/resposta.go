package controller

import (
	"auth"
	"db"
	"log"
	"model"
	"net/http"
	"view"
)

func RespostaTodas(w http.ResponseWriter, r *http.Request) {
	var v view.Model
	var id = r.FormValue("id")
	pergunta, _ := model.EncontarPerguntaPorID(id)
	respotas, err := model.ListarRespostasPorPergunta(id)
	v.Pergunta = pergunta
	v.Respostas = respotas
	if err != nil {
		v.Erro = err.Error()
	}
	usuario := auth.Get(r)
	if usuario != "" && usuario == model.EncontraAutorDaPergunta(id) {
		v.UserIsOP = true
	}
	v.Render(w, "respostas.html")
}

func RespostaNova(w http.ResponseWriter, r *http.Request) {
	var v view.Model
	if r.Method == "GET" {
		v.Resposta.Pergunta = r.FormValue("id")
		v.Render(w, "resposta.html")
		return
	}
	v.Resposta.Usuario = auth.Get(r)
	v.Resposta.Pergunta = r.FormValue("pergunta")
	v.Resposta.Texto = r.FormValue("texto")
	v.Resposta.Marcada = "0"
	err := v.Resposta.Salvar()
	if err != nil {
		v.Erro = err.Error()
		v.Render(w, "resposta.html")
	} else {
		http.Redirect(w, r, "/respostas?id="+r.FormValue("pergunta"), http.StatusFound)
	}
}

func RespostaMarcar(w http.ResponseWriter, r *http.Request) {
	userID := auth.Get(r)
	var posterID string
	err := db.Get(&posterID, "select usuario from pergunta where id = ?", r.FormValue("pergunta"))
	if err != nil {
		log.Println(err)
	}
	if posterID == userID {
		var v view.Model
		v.Resposta.ID = r.FormValue("resposta")
		v.Resposta.Pergunta = r.FormValue("pergunta")
		err := v.Resposta.Marcar()
		if err != nil {
			log.Println("MARCAR", err)
		}
	}
	redirectReferer(w, r)
}
