package model

import (
	"db"
)

func ListarTodosTopicos() ([]Topico, error) {
	var topicos []Topico
	err := db.Select(&topicos, "select * from topico")
	return topicos, err
}
