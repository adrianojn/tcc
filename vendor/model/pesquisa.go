package model

import "db"

func EncontarPerguntas(texto string) []Pergunta {
	var perguntas []Pergunta
	err := db.Select(&perguntas, `
		select distinct *
		from   pergunta
		where  titulo like '%' || ? || '%'
		or     texto  like '%' || ? || '%'
	`, texto, texto)
	logErr(err)
	return perguntas
}

func EncontarPerguntasERespostas(texto string) []Pergunta {
	var perguntas []Pergunta
	err := db.Select(&perguntas, `
		select distinct pergunta.*
		from   pergunta, resposta
		where  pergunta.titulo like '%' || ? || '%'
		or     pergunta.texto  like '%' || ? || '%'
		or    (resposta.texto  like '%' || ? || '%'
		and    resposta.pergunta = pergunta.id)
	`, texto, texto, texto)
	logErr(err)
	return perguntas
}

func EncontarPerguntasERespostasMarcadas(texto string) []Pergunta {
	var perguntas []Pergunta
	err := db.Select(&perguntas, `
		select distinct pergunta.*
		from   pergunta, resposta
		where  pergunta.titulo like '%' || ? || '%'
		or     pergunta.texto  like '%' || ? || '%'
		or    (resposta.texto  like '%' || ? || '%'
		and    resposta.pergunta = pergunta.id
		and    resposta.marcada  = 1)
	`, texto, texto, texto)
	logErr(err)
	return perguntas
}
