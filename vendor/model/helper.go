package model

import (
	"log"
	"strings"
	"time"
)

func empty(s string) bool {
	return strings.TrimSpace(s) == ""
}

func formatData(s string) string {
	t, err := time.Parse("2006-01-02T15:04:05Z", s)
	if err != nil {
		log.Println("FORMAT-DATA", err)
		return s
	}
	return t.Format("02/01/2006")
}

func logErr(err error) {
	if err != nil {
		log.Println(err)
	}
}
