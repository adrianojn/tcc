package model

import (
	"db"
	"errors"
)

func EncontrarRespostaPorID(id string) (Resposta, error) {
	var resposta Resposta
	err := db.Get(&resposta, "select * from resposta where id = ?", id)
	return resposta, err
}

func ListarRespostasPorPergunta(id string) ([]Resposta, error) {
	var respostas []Resposta
	err := db.Select(&respostas, `
		select   resposta.*, usuario.nome as usuario
		from     resposta, usuario
		where    resposta.usuario = usuario.id
		and      resposta.pergunta = ?
		order by marcada desc
	`, id)
	return respostas, err
}

func (r *Resposta) Salvar() error {
	if err := r.Validate(); err != nil {
		return err
	}
	_, err := db.Exec("insert into resposta (usuario, pergunta, texto, marcada) values (?, ?, ?, 0)",
		r.Usuario, r.Pergunta, r.Texto)
	return err
}

func (r *Resposta) Validate() error {
	if empty(r.Usuario) {
		return errors.New("Usuário inválido.")
	}
	if empty(r.Pergunta) {
		return errors.New("Pergunta inválida.")
	}
	if empty(r.Texto) {
		return errors.New("Texto não pode estar em branco.")
	}
	return nil
}

func (r *Resposta) IsMarcada() bool {
	return r.Marcada == "1"
}

func (r *Resposta) Marcar() error {
	if r.ID == "" {
		return errors.New("Resposta inválida.")
	}
	if r.Pergunta == "" {
		return errors.New("Pergunta inválida.")
	}
	_, err := db.Exec("update resposta set marcada = 0 where pergunta = ?", r.Pergunta)
	if err != nil {
		return err
	}
	_, err = db.Exec("update resposta set marcada = 1 where id = ?", r.ID)
	if err != nil {
		return err
	}
	return nil
}

func (r *Resposta) FormatData() string {
	return formatData(r.Data)
}
