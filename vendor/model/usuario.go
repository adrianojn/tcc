package model

import (
	"db"
	"errors"
)

func ListarTodosUsuarios() ([]Usuario, error) {
	var usuarios []Usuario
	err := db.Select(&usuarios, "select * from usuario")
	return usuarios, err
}

func EncontrarUsuarioPorID(id string) (Usuario, error) {
	var usuario Usuario
	err := db.Get(&usuario, "select * from usuario where id = ?", id)
	return usuario, err
}

func EncontrarUsuarioPorNome(nome string) (Usuario, error) {
	var usuario Usuario
	err := db.Get(&usuario, "select * from usuario where nome = ?", nome)
	return usuario, err
}

func EncontrarUsuarioPorEmail(email string) (Usuario, error) {
	var usuario Usuario
	err := db.Get(&usuario, "select * from usuario where email = ?", email)
	return usuario, err
}

func (u *Usuario) Salvar() error {
	_, err := db.Exec("insert into usuario (nome, hash, email, aprovado) values (?, ?, ?, 0)",
		u.Nome, u.Hash, u.Email)
	return err
}

func (u *Usuario) IsAprovado() bool {
	return u.Aprovado == "1"
}

func (u *Usuario) Validate() error {
	if empty(u.Nome) {
		return errors.New("Nome não pode estar em branco.")
	}
	if empty(u.Hash) {
		return errors.New("Senha inválida.")
	}
	if empty(u.Email) {
		return errors.New("E-mail não pode estar em branco.")
	}
	var email string
	err := db.Get(&email, "select email from usuario where email = ?", u.Email)
	if err != nil {
		return err
	}
	if email != "" {
		return errors.New("E-mail já está cadastrado.")
	}
	return nil
}
