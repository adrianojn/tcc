package model

type (
	Pergunta struct{ ID, Usuario, Topico, Titulo, Texto, Data string }
	Resposta struct{ ID, Usuario, Pergunta, Texto, Marcada, Data string }
	Topico   struct{ ID, Titulo, Texto, Ordem string }
	Usuario  struct{ ID, Nome, Hash, Email, Aprovado, Administrador string }
)
