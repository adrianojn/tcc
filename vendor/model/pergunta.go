package model

import (
	"db"
	"errors"
)

func EncontarPerguntaPorID(id string) (Pergunta, error) {
	var pergunta Pergunta
	err := db.Get(&pergunta, `
		select pergunta.*, usuario.nome as usuario
		from   pergunta, usuario
		where  pergunta.usuario = usuario.id
		and    pergunta.id = ?
	`, id)
	return pergunta, err
}

func EncontraAutorDaPergunta(id string) string {
	var autor string
	db.Get(&autor, `
		select usuario.id
		from   pergunta, usuario
		where  pergunta.usuario = usuario.id
		and    pergunta.id = ?
	`, id)
	return autor
}

func ListarPerguntasPorTopico(id string) ([]Pergunta, error) {
	var perguntas []Pergunta
	err := db.Select(&perguntas, `
		select   pergunta.*, usuario.nome as usuario
		from     pergunta, usuario
		where    pergunta.usuario = usuario.id
		and      pergunta.topico = ?
		order by data desc
	`, id)
	return perguntas, err
}

func (p *Pergunta) Salvar() error {
	if err := p.Validate(); err != nil {
		return err
	}
	_, err := db.Exec("insert into pergunta (usuario, topico, titulo, texto) values (?, ?, ?, ?)",
		p.Usuario, p.Topico, p.Titulo, p.Texto)
	return err
}

func (p *Pergunta) Validate() error {
	if empty(p.Usuario) {
		return errors.New("Usuário inválido.")
	}
	if empty(p.Topico) {
		return errors.New("Tópico inváido.")
	}
	if empty(p.Titulo) {
		return errors.New("Titulo não pode estar em branco.")
	}
	if empty(p.Texto) {
		return errors.New("Texto não pode estar em branco")
	}
	return nil
}

func (p *Pergunta) FormatData() string {
	return formatData(p.Data)
}
