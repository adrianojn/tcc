package auth

import (
	"net/http"
)

func MustBeUser(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if Get(r) == userNotFound {
			http.Redirect(w, r, "/entrar", http.StatusFound)
		}
		next(w, r)
	}
}
