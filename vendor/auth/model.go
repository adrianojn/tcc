package auth

import (
	"crypto/rand"
	"encoding/base64"
	"net/http"
)

var (
	loggedUsers  = make(map[string]string)
	userNotFound = ""
	userAdmin    = "1"
	userAprovado = "1"
)

func Get(r *http.Request) string {
	cookie, err := r.Cookie("token")
	if err != nil {
		return userNotFound
	}
	user, ok := loggedUsers[cookie.Value]
	if !ok {
		return userNotFound
	}
	return user
}

func Save(w http.ResponseWriter, user string) {
	b := make([]byte, 64)
	rand.Read(b)
	token := base64.URLEncoding.EncodeToString(b)

	loggedUsers[token] = user

	http.SetCookie(w, &http.Cookie{
		Name:     "token",
		Value:    token,
		HttpOnly: true,
	})
}

func Delete(w http.ResponseWriter) {
	http.SetCookie(w, &http.Cookie{
		Name:     "token",
		Value:    "",
		HttpOnly: true,
		MaxAge:   -1,
	})
}
