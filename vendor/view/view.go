package view

import (
	"html/template"
	"io"
	"log"
	"model"
)

var (
	files  = template.Must(template.ParseGlob("html/*.html"))
	Render = files.ExecuteTemplate
)

type Model struct {
	Erro string

	Pergunta  model.Pergunta
	Perguntas []model.Pergunta
	Resposta  model.Resposta
	Respostas []model.Resposta
	Topico    model.Topico
	Topicos   []model.Topico
	Usuario   model.Usuario
	Usuarios  []model.Usuario

	LoggedIn bool
	UserIsOP bool
}

func (m *Model) Render(w io.Writer, name string) {
	err := Render(w, name, m)
	if err != nil {
		log.Println("RENDER", err)
	}
}
