package bcrypt

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

func Encode(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 11)
	if err != nil {
		log.Println("BCRYPT", err)
	}
	return string(bytes), err
}

func Check(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false
	}
	return true
}
