package main

import (
	"auth"
	"controller"
	"db"
	"flag"
	"log"
	"net/http"
)

var get = http.HandleFunc

func main() {
	if commandLineAction() {
		return
	}

	get("/", controller.Index)
	get("/pesquisa", controller.Pesquisa)
	get("/aguarde", controller.Aguarde)

	get("/entrar", controller.Entrar)
	get("/sair", controller.Sair)

	get("/usuarios", controller.UsuarioTodos)
	get("/usuario/new", controller.UsuarioNovo)

	get("/topicos", controller.TopicoTodos)
	get("/perguntas", controller.PerguntaTodas)
	get("/respostas", controller.RespostaTodas)

	get("/pergunta/new", auth.MustBeUser(controller.PerguntaNova))
	get("/resposta/new", auth.MustBeUser(controller.RespostaNova))
	get("/marcar", auth.MustBeUser(controller.RespostaMarcar))

	log.Println("LISTENING")
	http.ListenAndServe(":3000", nil)
}

// commandLineAction permite execultar tarefas de administração
// usando a linha de comando.
// Retorna true se foi ultilizada.
// Aprovar usuário:
//    forum -id usuario_id
// Adicionar tópico:
//    forum -titulo 'Título do tópico' -texto 'Texto do tópico'
func commandLineAction() bool {
	var (
		usuario = flag.String("id", "", "ID do usuaário a ser aprovado.")
		titulo  = flag.String("titulo", "", "Título do novo tópico")
		texto   = flag.String("texto", "", "Texto do novo tópico")
	)
	flag.Parse()
	if *usuario != "" {
		db.Aprovar(*usuario)
		return true
	}
	if *titulo != "" && *texto != "" {
		db.NovoTopico(*titulo, *texto)
		return true
	}
	return false
}
